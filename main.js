// Set stems directory path
const stems_dir = '/home/julienbidoret/Code/www/stolon/stems/';

// Set Stolon URL
const root_url = "http://localhost/stolon";

// Debug
const debug = false;

// Thumb size
const thumb_width = 400, thumb_height = 225;

import puppeteer from 'puppeteer';
import gm from 'gm';
import fs from 'fs';
import {readdir} from 'fs/promises';

// Allow gm to use imageMagick
const im = gm.subClass({ imageMagick: true });

// prepare puppeterr
const browser = await puppeteer.launch();

// util : list directory
async function listDirectories(pth) {
  console.log(`Listing directories`);
  const directories = (await readdir(pth, {withFileTypes: true}))
    .filter(dirent => dirent.isDirectory())
    .map(dir => dir.name);
  return directories;
}

// util : resize image
const resizeImage = (input, output, width, height) => {
  im(input)
    .resize(width, height)
    .write(output, (err) => {
        if(err){
            console.log(err);
        } else {
          // delete screenshot after conversion
          fs.rm(input,()=>{})
        }
    });
};


// list stems
listDirectories(stems_dir).then( async function(stems){
  await processStems(stems)
}).then( async () => {
  await browser.close();
});

// async process each stem
async function processStems(stems){
  for (let stem of stems) { 
    await takeScreenshot(stem);
  }
}

// take screenshot, and generate thumb
async function takeScreenshot(stem){
  const url = `${root_url}/stems/${stem}/index.html`;
  const stem_dir = debug ? `screenshots/${stem}` : `${stems_dir}/${stem}`;
  const screenshot_path = `${stem_dir}/thumb.png`;
  const thumb_path = `${stem_dir}/stem.webp`;

  console.log('---');
    
  // create dir if needed
  if (!fs.existsSync(stem_dir)) {
    fs.mkdirSync(stem_dir);
  } 

  // take screenshot then create thumb
  if (!fs.existsSync(thumb_path) || debug == true) {
    console.log(`Processing ${stem}.`);
    console.log(`  Preparing page…`);
    const page = await browser.newPage();
    await page.setViewport({ width: 1280, height: 720 });

    try {          
      console.log(`  Loading page…`);
      await page.goto(url);
      console.log(`  Page loaded.`);
    } catch(error){
      console.log(`  Error while loading page :(`);
    }
    
    try {          
        await page.screenshot({ path: screenshot_path });  
        console.log(`  Screenshot!`);        
        resizeImage(screenshot_path, thumb_path, thumb_width, thumb_height);
        console.log(`  Thumb generated!`);          
    } catch (error) {
      console.log(`Error with ${stem}`);
      return false;
    }

  } else {
    console.log(`${stem} has been processed`);
  }
 
}


