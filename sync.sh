#!/bin/bash

# This helper script allow to sync stems between a remote server and a local one
# Warning: it deletes non-existing stems when syncing

echo "Syncing stems"

local_stems_path="/your/local/stolon_dir/stems/"
remote_stems_path="/your/remote/stolon_dir/stems/" 
ssh_login="you" 
ssh_host="yourserver.com"
port="22"

ltr() {
  rsync -avz --delete --progress -e "ssh -p $port" $local_stems_path $ssh_login@$ssh_host:$remote_stems_path 
}

rtl() {
  rsync -avz --delete --progress -e "ssh -p $port" $ssh_login@$ssh_host:$remote_stems_path $local_stems_path
}

echo "Do you want to sync…"
echo "  1) From local to remote"
echo "  2) From remote to local"

read n
case $n in
  1) 
    echo "Uploading local stems"
    ltr
    ;;
  2) 
    echo "Downloading remote stems"
    rtl
    ;;
  *) 
    echo "Invalid option";;
esac