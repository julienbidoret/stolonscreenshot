#!/bin/bash

# This helper script allows to download remote stems, run the screenshoting process locally, then upload the gernerated thubmnails

local_stems_path="/your/local/stolon_dir/stems/"
remote_stems_path="/your/remote/stolon_dir/stems/" 
ssh_login="you" 
ssh_host="yourserver.com"

echo "Downloading remote stems"
echo "------------------------"
rsync -avz --progress -e "ssh -p $port" $ssh_login@$ssh_host:$remote_stems_path $local_stems_path

echo "Processing thumbnails"
echo "------------------------"
node main.js

echo "Uploading local thumbs"
echo "------------------------"
rsync -avz --progress -e "ssh -p $port" $local_stems_path $ssh_login@$ssh_host:$remote_stems_path 
