# Stolon screenshots

POC node.js script to generate screenshots for [Stolon](https://gitlab.com/raphaelbastide/stolon).

## Install dependencies

The two dependencies are [puppeteer](https://pptr.dev/guides/screenshots/) and [gm](https://aheckmann.github.io/gm/).

```
npm install
```

## Config

Set stems directory path and Stolon url in `main.js`.

```js
// Set stems directory path
const stems_dir = '/var/www/stolon/stems/';

// Set Stolon URL
const root_url = "http://localhost/stolon";
```

## Run
```
node main.js
```

## Helpers

You can run `sync.sh` that sync stems between a remote server and a local one.

You can run `main.sh` that download remote stems, run the screenshoting process locally, then upload the gernerated thubmnails.